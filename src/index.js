const chalk = require('chalk')
const {destFolderPrompt, ftpServerPrompts} = require('./modules/cliPrompts')
const ftpConnection = require('./modules/ftpConnection')

console.log(`\n Starting ${chalk.green.bold('whatsapp-media')}`)

const start = async () => {
    const destFolder = await destFolderPrompt()
    console.log('\n', chalk.yellow(`${chalk.bold('WARNING:')} The root folder of your FTP server has to be the WhatsApp Media folder`))

    console.log(chalk.yellow(` For example: ${chalk.bold('/storage/emulated/0/WhatsApp/Media')} \n`))

    const serverData = await ftpServerPrompts()
    console.log(serverData)
    await ftpConnection(serverData, destFolder)
}

start()