const isNameSupported = (name) => {
    const splited = name.split('-')

    if (splited.length != 3) { return false }

    const date = !isNaN(Number(splited[1])) && splited[1].length == 8 ? splited[1] : false

    if (!date) {return false}

    const month = Number(date.slice(4,6)) != 0 && Number(date.slice(4,6)) <= 12 ? true : false

    if (!month) {return false}

    return true
}

module.exports = isNameSupported