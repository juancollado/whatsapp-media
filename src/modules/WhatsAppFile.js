const { mkdir } = require('fs').promises

const Months = {
    '01': '1_January',
    '02': '2_February',
    '03': '3_March',
    '04': '4_April',
    '05': '5_May',
    '06': '6_June',
    '07': '7_July',
    '08': '8_August',
    '09': '9_September',
    '10': '10_October',
    '11': '11_November',
    '12': '12_December'
}

class WhatsAppFile {
    constructor(name, destFolder) {

        // Here is used the name of WhatsApp give to the files to determinate the year and the month
        this.name = name

        this.destFolder = destFolder

        // The 4 firts numbers after IMG- are the year
        this.year = name.split('-')[1].substr(0, 4)

        // The 2 number after the year are the month
        this.month = Months[name.split('-')[1].substr(4, 2)]

        // With the year and the month is determinated wich will the folder to import the file
        this.dirToImport = `${destFolder}${this.year}/${this.month}/${name}`

    }

    checkYearFolderExistence = async (currentDirectories) => {
        // If the year of the file it's not in the currentDirectories object
        if (!currentDirectories[this.year]) {

            try {
                //Create the folders where the file will be imported
                await mkdir(`${this.destFolder}${this.year}/${this.month}`, { recursive: true })
                return { succes: true, newItem: true } //newItem true when both folders are created
            } catch (error) {
                console.error(error)
                return { succes: false }
            }
        } else {
            return await this.checkMonthFolderExistence(currentDirectories[this.year])
        }
    }

    checkMonthFolderExistence = async (currentDirectories) => {
        // The year folder already exist but not the month folder
        if (!currentDirectories.includes(this.month)) {
            try {
                // The month folder is created
                await mkdir(`${this.destFolder + this.year}/${this.month}`)
                return { succes: true, newItem: false } //newItem false when only the month folder is created
            } catch (error) {
                // If the directory already exist
                if (error.errno == -17) {
                    return { succes: true }
                }
                console.error('HERE: ', error)
                return { succes: false }
            }
        }
        return { succes: true }  //newItem undefined when both folders already exist
    }
}

module.exports = WhatsAppFile
/*
Example:

const fileData = new WhatsAppFile('IMG-20171001-WA0001.jpg', '/home/user/Images')
console.log(fileData)

OUTPUT:
    WhatsAppFile {
    name: 'IMG-20171001-WA0001.jpg',
    year: '2017',
    month: '10_October',
    dirToImport: '/home/user/Images/2017/10_October/IMG-20171001-WA0001.jpg'
    }
*/
